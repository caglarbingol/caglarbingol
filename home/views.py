from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    return render(request,'home/index.html')

def about(request):
    return render(request,'home/about.html')

def portfolio(request):
    return render(request,'home/portfolio.html')

def contact(request):
    return render(request,'home/contact.html')