from django.http import HttpResponse, HttpResponseRedirect
from django.http import Http404
from django.shortcuts import get_object_or_404,render
from django.template import loader
from .models import Comment, Post
import datetime
from django.urls import reverse

# Create your views here.
def index(request):
    latest_post_list = Post.objects.order_by('-date')[:5]
    context = {'latest_post_list': latest_post_list}
    return render(request, 'blog/index.html', context)

def postpage(request, post_id):
    if request.method == "POST":
        post = get_object_or_404(Post, pk=post_id)
        sender_name = request.POST['sender_name']
        if not sender_name:
            return render(request, 'blog/postpage.html', {
                'post': post,
                'error_message': "Gonderici adi girilmedi. ",
            })
        comment = request.POST['comment']
        if not comment:
            return render(request, 'blog/postpage.html', {
                'post': post,
                'error_message': "Yorum girilmedi. ",
            })
        comment_id = request.POST['comment_id']
        parent = Comment.objects.get(pk=comment_id)
        if not parent:
            post.comment_set.create(sender_name= sender_name, context = comment , date = datetime.datetime.now())
        else:
            post.comment_set.create(sender_name= sender_name, context = comment , date = datetime.datetime.now(),parent= parent)
        #return render(request, 'blog/postpage.html', {'post': post})#
        return HttpResponseRedirect(reverse('blog:postpage', args=(post.id,)))
    post = get_object_or_404(Post, pk=post_id)
    return render(request, 'blog/postpage.html', {'post': post})