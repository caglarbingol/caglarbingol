from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
import datetime

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=64)
    context = models.CharField(max_length=1500)
    date = models.DateTimeField()
    
    def __str__(self):
        return f"{self.title} to {self.context}"

class Category(models.Model):
    title = models.CharField(max_length=64)
    posts = models.ManyToManyField(Post)

    def __str__(self):
        return self.title

class Comment(MPTTModel):
    sender_name = models.CharField(max_length=64)
    context = models.CharField(max_length=500)
    date = models.DateTimeField()
    post = models.ForeignKey(Post, on_delete = models.CASCADE)
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['-date']

    def __str__(self):
        return self.context